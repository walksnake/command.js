#command.js

基于jQuery的Command模式插件。

该插件将页面元素看成是一个个的对象，通过插件给对象赋予特定的状态，并定义特定的Command。一旦发生关联操作的时候，可以通过通知修改状态或者发布命令的方式进行关联操作。
