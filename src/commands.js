;(function($){
	function isEmpty(v){
		if(typeof(v) == 'undefined' || !v || $.trim(v) == ''){
			return true;
		}
		return false;
	}

	function find_state($elem){
		var opts = $elem.data("commands_options");
		if(isEmpty(opts))return;

		var state = $elem.data("commands_state");
		if(!isEmpty(state)){
			return state
		}else{
			return false;
		}
	}

	function drop_state($elem,oldState){
		if(!isEmpty(oldState)){
			var opts = $elem.data("commands_options");
			if(isEmpty(opts))return;

			if(!isEmpty(opts) && !isEmpty(opts.states) && opts.states.length > 0){
				for(var i = 0; i < opts.states.length; i++){
					var s = opts.states[i];
					if(!isEmpty(s.state) && s.state == oldState){
						if(!isEmpty(s.drop)){
							s.drop($elem,oldState);
						}
						$elem.data("commands_state",false);
					}
				}
			}

		}
	}

	function set_state($elem,state){
		var opts = $elem.data("commands_options");
		if(isEmpty(opts))return;

		drop_state($elem,find_state($elem));		// drop 当前的状态

		if(!isEmpty(opts.states) && opts.states.length > 0){
			for(var i = 0; i < opts.states.length; i++){
				var s = opts.states[i];
				if(!isEmpty(s.state) && s.state == state){
					set_state_($elem,s);
				}
			}
		}
	}

	function set_state_($elem,s){
		if(!isEmpty(s.before)){
			s.before($elem,s.state);
		}
		if(!isEmpty(s.paint)){
			s.paint($elem,s.state);
		}
		$elem.data("commands_state",s.state);
		if(!isEmpty(s.after)){
			s.after($elem,s.state);
		}
	}

	function exec_command($elem,command,args){
		var opts = $elem.data("commands_options");
		if(isEmpty(opts))return;

		if(!isEmpty(opts) && !isEmpty(opts.commands)){
			for(var i = 0; i < opts.commands.length; i++){
				var c = opts.commands[i];
				if(!isEmpty(c.name) && c.name == command){
					if(!isEmpty(c.action)){
						c.action($elem,command,args);
					}
				}
			}
		}
	}

	function init($elem){
		var opts = $elem.data("commands_options");
		if(isEmpty(opts))return;

		drop_state(find_state($elem));		// drop 当前的状态

		if(!isEmpty(opts.states) && opts.states.length > 0){
			for(var i = 0; i < opts.states.length; i++){
				var s = opts.states[i];
				if(!isEmpty(s) && typeof(s.isDefault) != 'undefined' && s.isDefault){
					set_state_($elem,s);
				}
			}
		}

		if(!isEmpty(opts.init)){
			opts.init($elem);
		}
	}

	function mergeOptions(opts1,opts2){
		// 合并states
		var opts = {};
		if(!isEmpty(opts2.states)){
			opts.states = opts2.states;
			for(var i = 0; i < opts1.states.length; i++){
				var s1 = opts1.states[i];
				var isContains = false;
				for(var j = 0; j < opts2.states.length;j++){
					var s2 = opts1.states[j];
					if(s2.state == s1.state) isContains = true;
					break;
				}

				if(!isContains) opts.states[opts.states.length] = s1;
			}
		}else{
			opts.states = opts1.states;
		}

		if(!isEmpty(opts2.commands)) {
			opts.commands = opts2.commands;
			for (var i = 0; i < opts1.commands.length; i++) {
				var c1 = opts1.commands[i];
				var isContains = false;
				for (var j = 0; j < opts2.commands.length; j++) {
					var c2 = opts1.commands[j];
					if (c2.name == c1.name) isContains = true;
					break;
				}

				if (!isContains) opts.commands[opts.commands.length] = c1;
			}
		}else {
			opts.commands = opts1.commands;
		}

		opts.init = (isEmpty(opts1.init) ? (isEmpty(opts2.init) ? undefined : opts2.init) : opts1.init);

		return opts;
	}

	$.fn.commands = function(options){
		if (typeof options === 'string') {
			options = {};
		}
		var opts =  $.extend({}, $.fn.commands.defaults, options);

		// 如果已经存在，则用新的options继承已经存在的options
		var oldOpts = $(this).data("commands_options");
		if(!isEmpty(oldOpts)){
			opts =  mergeOptions(opts,oldOpts);
		}else{
			opts = mergeOptions(opts,{});
		}

		return this.each(function(){
			var $elem = $(this);
			$elem.data("commands_options",opts);
			init($elem);
		});
	}

	$.fn.commands.defaults = {
		states : [],
		commands : []
	};

	// 设置状态
	$.fn.command = function(command){
		if (typeof(command) === 'string' || $.trim(command) !== '') {
			var params = [];
			for(var i = 1;i < arguments.length;i++){
				params[i -1] = arguments[i];
			}

			exec_command($(this),command,params);
		}
		return this.each(function(){});
	}

	$.fn.state = function(s){
		if (typeof(s) === 'string' || $.trim(s) !== '') {
			set_state($(this),s);
		}
		return this.each(function(){});
	}

	$.fn.currentState = function(){
		return find_state($(this));
	}
})(jQuery);